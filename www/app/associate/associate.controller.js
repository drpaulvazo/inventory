(function () {
  'use strict';

  angular
    .module('inventoryApp')
    .controller('AssociateController', AssociateController);

  AssociateController.$inject = ['$state', 'staffService', 'logger',
    'barcodeService', '$ionicPopup', '$scope', 'objectService'];

  function AssociateController($state, staffService, logger, barcodeService,
                               $ionicPopup, $scope, objectService) {
    //Variables

    var vm = this;
    vm.person = [];
    vm.barcode = "";
    //Functions

    vm.goToHome = goToHome;
    vm.swipe = swipe;

    active();

    objectService.getObjects().then(function (data) {

    });
    /////////////////////////////

    function active() {
      vm.barcode = barcodeService.currentBarcode;
      return getPerson(staffService.idUser).then(function () {
        logger.info('Actived Associate View');
      });
    }

    function getPerson(id) {
      return staffService.getPerson(id).then(function (data) {
          vm.person = data;
          logger.info('person',vm.person);
          return vm.person;
        }
      );
    }

    function swipe() {
      barcodeService.currentBarcode = vm.barcode;
      var isValidObject = objectService.isObject(vm.barcode);
      console.log(isValidObject);
      if (vm.barcode == "" || !isValidObject) {
        var messageError = 'Enter the Code';
        if(!isValidObject){
          messageError = 'The object '+vm.barcode+ ' isn\'t valid';
        }
        $ionicPopup.show({
          title: 'Barcode Error',
          subTitle: messageError,
          scope: $scope,
          buttons: [
            {
              text: 'OK', onTap: function (e) {
              return false;
            }
            },
          ]
        });
      } else {
        $ionicPopup.show({
          title: 'Associate',
          subTitle: 'Confirm associate <br>' + barcodeService.currentBarcode,
          scope: $scope,
          buttons: [
            {
              text: 'Cancel', onTap: function (e) {
              return false;
            }
            },
            {
              text: '<b>Save</b>',
              type: 'button-positive',
              onTap: function (e) {
                return true;
              }
            },
          ]
        }).then(function (res) {
          if (res) {
            objectService.associate(vm.barcode, vm.person.serialNumber).then(function (data) {
              barcodeService.currentBarcode = '';
            });
            $state.go('home');
          }
        }, function (err) {
        }, function (msg) {
        });
      }
    }

    function goToHome() {
      $state.go('home');
    }

  }
})();
