(function () {
  'use strict';

  angular
    .module('app.associate')
    .config(configuration);

  configuration.$inject = ['$stateProvider'];

  function configuration($stateProvider) {

    var states = [
      {
        name: 'associate',
        url: '/associate',
        templateUrl: 'app/associate/associate.html',
        controller: 'AssociateController',
        controllerAs: 'associate'
      }
    ];

    states.forEach($stateProvider.state);

  }

})();
