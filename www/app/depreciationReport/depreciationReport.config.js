(function () {
  'use strict';

  angular
    .module('app.depreciationReport')
    .config(configuration);

  configuration.$inject = ['$stateProvider'];

  function configuration($stateProvider) {

    var states = [
      {
        name: 'depreciationReport',
        url: '/depreciationReport',
        templateUrl: 'app/depreciationReport/depreciationReport.html',
        controller: 'depreciationReportController',
        controllerAs: 'depreciationReport'
      }
    ];

    states.forEach($stateProvider.state);

  }

})();
