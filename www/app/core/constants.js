/* global toastr:false, moment:false */
(function() {
  'use strict';

  angular
    .module('app.core')
    .constant('toastr', toastr)
    .constant('host',"http://localhost")
    .constant('port',"8080")
    .constant('apiKey',"7B4BCCDC");
})();
