(function () {
  'use strict';

  angular
    .module('app.core', [
      'ionic',
      'ngCordova',
      'ion-floating-menu',
      'aCarousel',
      'tabSlideBox',
      'blocks.logger',
      'restangular',
      'angularChart'
    ]);
})();
