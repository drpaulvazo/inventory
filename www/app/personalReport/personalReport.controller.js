(function () {
  'use strict';

  angular
    .module('inventoryApp')
    .controller('PersonalReportController', PersonalReportController);

  PersonalReportController.$inject = ['$state','staffService','logger'];

  function PersonalReportController($state,staffService,logger) {

    var vm = this;
    //variables
    vm.staff = [];

    //Functions
    vm.goToPerfil = goToPerfil;

    active();

    /////////////////////////////

    function active(){
      return getStaff().then(function (){
        logger.info('Actived Staff Report View');
      })
    }

    function getStaff(){
      return staffService.getStaff().then(function(data){
          logger.info('getDataStaff',data);
          vm.staff = data;
          return vm.staff;
        }
      )
    }
    function goToPerfil(currentPerson){
      logger.info('Current Person',currentPerson);
      staffService.idUser = currentPerson;
      $state.go('perfil');
    }
  }
})();
