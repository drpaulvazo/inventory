(function () {
  'use strict';

  angular
    .module('app.personalReport')
    .config(configuration);

  configuration.$inject = ['$stateProvider'];

  function configuration($stateProvider) {

    var states = [
      {
        name: 'personalReport',
        url: '/personalReport',
        templateUrl: 'app/personalReport/personalReport.html',
        controller: 'PersonalReportController',
        controllerAs: 'personalReport'
      }
    ];

    states.forEach($stateProvider.state);

  }

})();
