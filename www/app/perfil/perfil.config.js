(function () {
  'use strict';

  angular
    .module('app.perfil')
    .config(configuration);

  configuration.$inject = ['$stateProvider'];

  function configuration($stateProvider) {

    var states = [
      {
        name: 'perfil',
        url: '/perfil',
        templateUrl: 'app/perfil/perfil.html',
        controller: 'PerfilController',
        controllerAs: 'perfil'
      }
    ];

    states.forEach($stateProvider.state);

  }

})();
