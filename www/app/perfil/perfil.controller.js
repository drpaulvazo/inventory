(function () {
  'use strict';

  angular
    .module('inventoryApp')
    .controller('PerfilController', PerfilController);

  PerfilController.$inject = ['$scope', '$ionicScrollDelegate', 'staffService', 'logger',
    '$ionicSlideBoxDelegate','objectService'];

  function PerfilController($scope, $ionicScrollDelegate, staffService, logger,
                            $ionicSlideBoxDelegate,objectService) {
    var vm = this;

    //variables
    vm.person = [];
    vm.objects = [];
    //functions

    vm.onUserDetailContentScroll = onUserDetailContentScroll;
    vm.getIcon = getIcon;
    vm.disassociate = disassociate;
    activate();

    /////////////////////////////////////////////////////////
    //icon-008-laptop

    function getIcon(typeName) {
      if (typeName == "Monitor") {
        return "icon-006-technology";
      }
      if (typeName == "laptop") {
        return "icon-008-laptop";
      }
    }

    function activate() {
      $ionicSlideBoxDelegate.update();
      return getPerson(staffService.idUser).then(function () {

        logger.info('Object', vm.objects);
        logger.info('Actived Associate View', vm.person);
      });
    }

    function getPerson(id) {
      return staffService.getPerson(id).then(function (data) {
          vm.person = data;
          logger.info('person',vm.person);
          getObjectsByPerson(vm.person.serialNumber);
          return vm.person;
        }
      );
    }

    function getObjectsByPerson(codeEmployee) {
      return objectService.getObjectsByPerson(codeEmployee).then(function (data) {
        vm.objects = data;
        $ionicSlideBoxDelegate.update();
        return data;
      });
    }

    function onUserDetailContentScroll() {
      var scrollDelegate = $ionicScrollDelegate.$getByHandle('userDetailContent');
      var scrollView = scrollDelegate.getScrollView();
      $scope.$broadcast('userDetailContent.scroll', scrollView);
    }
    function disassociate(object){
      logger.info('object',object);
      objectService.setCurrentObject(object)
      logger.info('dis',objectService.currentObject);
      objectService.associate(object.serialNumber, "IT").then(function (data) {
        getObjectsByPerson(vm.person.serialNumber);
      });
    }
  }
})();
