(function () {
  'use strict';

  angular
    .module('app.damageReport')
    .config(configuration);

  configuration.$inject = ['$stateProvider'];

  function configuration($stateProvider) {

    var states = [
      {
        name: 'damageReport',
        url: '/damageReport',
        templateUrl: 'app/damageReport/damageReport.html',
        controller: 'damageReportController',
        controllerAs: 'damageReport'
      }
    ];

    states.forEach($stateProvider.state);

  }

})();
