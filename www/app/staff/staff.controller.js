(function () {
  'use strict';

  angular
    .module('inventoryApp')
    .controller('StaffController', StaffController);

  StaffController.$inject = ['$state', 'staffService', 'logger'];
  function StaffController($state, staffService, logger) {
    //variables
    var vm = this;
    vm.staff = [];

    //functions
    vm.goAssociate = goAssociate;

    active();

    //////////////////////

    function goAssociate(name) {
      logger.info(name);
      staffService.idUser = name;
      $state.go('associate');
    }

    function active() {
      return getStaff().then(function () {
        logger.info('Actived Staff View');
      });
    }

    function getStaff() {
      return staffService.getStaff().then(function (data) {
          logger.info('getDataStaff', data);
          console.log(data);
          vm.staff = data;
          return vm.staff;
        }
      );
    }
  }
})();
