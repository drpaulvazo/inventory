(function () {
  'use strict';

  angular
    .module('app.staff')
    .config(configuration);

  configuration.$inject = ['$stateProvider'];

  function configuration($stateProvider) {

    var states = [
      {
        name: 'staff',
        url: '/staff',
        templateUrl: 'app/staff/staff.html',
        controller: 'StaffController',
        controllerAs: 'staff'
      }
    ];

    states.forEach($stateProvider.state);

  }

})();
