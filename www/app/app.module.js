(function () {
  'use strict';
  angular
    .module('inventoryApp', [
      'app.core',
      'app.config',
      'app.state',
      'app.auth',
      'app.login',
      'app.home',
      'app.staff',
      'app.associate',
      'app.personalReport',
      'app.perfil',
      'app.damageReport',
      'app.depreciationReport'
    ]);
})();
