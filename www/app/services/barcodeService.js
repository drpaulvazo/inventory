(function() {
  'use strict';

  angular
    .module('app.core')
    .factory('barcodeService', barcodeService);

  /* @ngInject */
  function barcodeService($http, $location, $q, logger) {
    var isPrimed = false;
    var primePromise;
    var currentBarcode = "";
    var service = {
      ready: ready,
      currentBarcode:currentBarcode
    };

    return service;

    function prime() {
      // This function can only be called once.
      if (primePromise) {
        return primePromise;
      }

      primePromise = $q.when(true).then(success);
      return primePromise;

      function success() {
        isPrimed = true;
        logger.info('Primed data');
      }
    }

    function ready(nextPromises) {
      var readyPromise = primePromise || prime();
      return readyPromise
        .then(function() { return $q.all(nextPromises); })
        .catch(debugMode.error('"ready" function failed'));
    }

  }
})();
