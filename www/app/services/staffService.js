(function() {
  'use strict';

  angular
    .module('app.core')
    .factory('staffService', staffService);

  /* @ngInject */
  function staffService($http, $location, $q, logger,restService,Restangular) {
    var isPrimed = false;
    var primePromise;
    var currentPerson = [];
    var idUser = "";
    var service = {
      getStaff: getStaff,
      getPerson: getPerson,
      ready: ready,
      currentPerson:currentPerson,
      idUser:idUser
    };

    return service;



    function getStaff() {

      return restService.configureFirst().all('things/?where=thingTypeId%3D19').customGET("")
        .then(handleSuccess)
        .catch(handleError);

      function handleSuccess(response) {
        return response.results;
      }
      function handleError(reason) {
        return reason;
      }
    }

    function setPObject(codeEmployee) {

      return restService.configureFirst().all('thing/201').customGET("")
        .then(handleSuccess)
        .catch(handleError);

      function handleSuccess(response) {
        return response.results;
      }
      function handleError(reason) {
        return reason;
      }
    }

    function getPerson(id){
      //http://localhost:8080/riot-core-services/api/things/?where=thingTypeId%3D16&extra=children&upVisibility=false&treeView=true
      return restService.configureFirst().all('things/?where=thingTypeId%3D19').customGET("")
        .then(handleSuccess)
        .catch(function(message) {
            logger.error('Failed Get Staff',message);
            $location.url('/');
        });
      function handleSuccess(response) {
        for(var i=0;i<response.results.length;i++){
          if(response.results[i].name == id){
            currentPerson =response.results[i];
          }
        }
        return currentPerson;
      }
    }
/*
    function updatePerson(number){
      body.serialNumber = currentPerson.
      return restService.configureFirst().all('thing').customPATCH(body, currentPerson._id)
        .then(handleSuccess)
        .catch(function(message) {
          logger.error('Failed Get Staff',message);
          $location.url('/');
        });
      function handleSuccess(response) {

        return currentPerson;
      }

    }*/
    function prime() {
      // This function can only be called once.
      if (primePromise) {
        return primePromise;
      }

      primePromise = $q.when(true).then(success);
      return primePromise;

      function success() {
        isPrimed = true;
        logger.info('Primed data');
      }
    }

    function ready(nextPromises) {
      var readyPromise = primePromise || prime();
      return readyPromise
        .then(function() { return $q.all(nextPromises); })
        .catch(debugMode.error('"ready" function failed'));
    }

  }
})();



