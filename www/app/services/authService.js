(function () {
  'use strict';

  angular
    .module('app.auth')
    .factory('authService', authService);

  authService.$inject = ['$q', 'Restangular', 'restService','logger'];

  function authService($q, Restangular, restService,logger) {

    var
      service = {
        authenticate: authenticate,
      };

    return service;


    function authenticate(credentials) {

      var deferred = $q.defer();

      restService.configureFirst().all('user/login').post(credentials)
        .then(handleSuccess)
        .catch(handleError);

      function handleSuccess(response) {
        Restangular.configuration.defaultHeaders.token = response.token;
        deferred.resolve(response);
      }

      function handleError(reason) {
        logger.error(reason.statusText,reason);
        deferred.reject(reason);
      }

      return deferred.promise;

    }

  }

})();
