(function() {

  angular
    .module('app.core')
    .factory('restService', restService);

  restService.$inject = ['Restangular','host','port','logger','apiKey'];

  function restService(Restangular,host,port,logger,apiKey) {

    return {
      configure: configure,
      configureFirst: configureFirst
    };

    function configure() {

      return Restangular.withConfig(config);

      function config(RestangularConfigurer) {

        var baseUrl = host + ':' + port + "/riot-core-services/api/";

        RestangularConfigurer.setBaseUrl(baseUrl);

        RestangularConfigurer.setDefaultHeaders({
          'Compress': 'True',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'apikey':apiKey
        });

      }

    }

    function configureFirst() {

      return Restangular.withConfig(config);

      function config(RestangularConfigurer) {
        RestangularConfigurer.setDefaultHeaders({
          'Compress': 'True',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'api_key':apiKey
        });
        var baseUrl = host + ':' + port + "/riot-core-services/api/";
        RestangularConfigurer.setBaseUrl(baseUrl);

      }

    }
  }

})();
