(function () {
  'use strict';

  angular
    .module('app.core')
    .factory('objectService', objectService);

  /* @ngInject */
  function objectService($http, $location, $q, logger, restService) {
    var isPrimed = false;

    var body = {
      "group": ">CODEROAD",
      "name": "Keyboard",
      "thingTypeCode": "Objeto",
      "serialNumber": "",
      "udfs": {
        "codeEmployee": {
          "value": ""
        }
      }
    }
    var primePromise;
    var objects = [];
    var currentObject = [];
    var service = {
      ready: ready,
      objects: objects,
      getObjects: getObjects,
      associate: associate,
      isObject: isObject,
      getObjectsByPerson: getObjectsByPerson,
      setCurrentObject: setCurrentObject
    };

    return service;

    function prime() {
      // This function can only be called once.
      if (primePromise) {
        return primePromise;
      }

      primePromise = $q.when(true).then(success);
      return primePromise;

      function success() {
        isPrimed = true;
        logger.info('Primed data');
      }
    }

    function ready(nextPromises) {
      var readyPromise = primePromise || prime();

      return readyPromise
        .then(function () {
          return $q.all(nextPromises);
        })
        .catch(debugMode.error('"ready" function failed'));
    }

    function getObjects() {
      return restService.configureFirst().all('things').customGET('', {
        where: "thingTypeId=18",
        only: "_id,name,thingTypeName,serialNumber"
      })
        .then(getStaffComplete)
        .catch(function (message) {
          logger.error('Failed Get Staff', message);
          $location.url('/');
        });

      function getStaffComplete(data) {
        logger.info('objects', data.results);
        objects = data.results;
        return data.results;
      }
    }

    function isObject(serialNumber) {
      console.log(serialNumber);
      for (var i = 0; i < objects.length; i++) {
        console.log(objects[i]);
        if (objects[i].serialNumber == serialNumber) {
          currentObject = objects[i];
          return true;
        }
      }
      return false;
    }

    function associate(serial, codeEmployee) {
      body.serialNumber = serial;
      body.udfs.codeEmployee.value = codeEmployee;
      logger.info('objectCurrent',currentObject)
      return restService.configureFirst().all('thing').customPATCH(body, currentObject._id)
        .then(handleSuccess)
        .catch(handleError);
      function handleSuccess(response) {
        return response.results;
      }

      function handleError(reason) {
        return reason;
      }
    }

    function getObjectsByPerson(codeEmployee) {
      return restService.configureFirst().all('things').customGET('', {
        where: "thingTypeId=18&codeEmployee.value=" + codeEmployee,
        only: "_id,name,thingTypeName,serialNumber,typeObject"
      })
        .then(getStaffComplete)
        .catch(function (message) {
          logger.error('Failed Get Staff', message);
          $location.url('/');
        });

      function getStaffComplete(data) {
        logger.info('objects', data.results);
        objects = data.results;
        return data.results;
      }
    }
    function setCurrentObject(object){
      currentObject = object;
    }
  }
})();
