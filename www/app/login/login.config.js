(function () {
  'use strict';

  angular
    .module('app.login')
    .config(configuration);

  configuration.$inject = ['$stateProvider'];

  function configuration($stateProvider) {

    var states = [
      {
        name: 'login',
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginController',
        controllerAs: 'login'
      }
    ];

    states.forEach($stateProvider.state);

  }

})();
