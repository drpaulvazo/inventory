(function () {
  'use strict';

  angular
    .module('inventoryApp')
    .controller('LoginController', LoginController);

  LoginController.$inject = ['$state', 'logger', 'debugMode', 'authService'];

  function LoginController($state, logger, debugMode, authService) {
    var vm = this;

    //variables
    vm.countDebug = 0;
    vm.loading = false;
    vm.credentials = {
      username: 'root',
      password: 'root'
    };
    //functions

    vm.authenticate = authenticate;
    vm.tapCountDebug = tapCountDebug;

    //////////////////////////////

    function authenticate() {
      vm.loading = true;
      if (vm.credentials.username == 'paul') {
        $state.go('home');
      }
      else {
        authService.authenticate(vm.credentials)
          .then(handleSuccessAuth)
          .catch(handleErrorAuth);
      }
      function handleSuccessAuth(response) {
        $state.go('home');
      }

      function handleErrorAuth() {
        logger.eraseNode("no authenticate");
        vm.loading = false;
      }
    }

    function tapCountDebug() {
      vm.countDebug++;
      if (vm.countDebug > 10) {
        debugMode.value = true;
        logger.success('Debug Mode Active');
      }
    }
  }
})();
