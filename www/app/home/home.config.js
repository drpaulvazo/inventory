(function () {
  'use strict';

  angular
    .module('app.home')
    .config(configuration);

  configuration.$inject = ['$stateProvider'];

  function configuration($stateProvider) {

    var states = [
      {
        name: 'home',
        url: '/home',
        templateUrl: 'app/home/home.html',
        controller: 'HomeController',
        controllerAs: 'home'
      }
    ];

    states.forEach($stateProvider.state);

  }

})();
