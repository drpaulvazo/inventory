(function () {
  'use strict';

  angular
    .module('inventoryApp')
    .controller('HomeController', HomeController);

  HomeController.$inject = ['$state','$cordovaBarcodeScanner','$ionicPlatform','logger','barcodeService'];

  function HomeController($state,$cordovaBarcodeScanner, $ionicPlatform,logger,barcodeService) {

    //variables
    var vm = this;
    vm.textScan = "barcode";
    vm.isInventory = true;

    //declare functions
    vm.scanRfid = scanRfid;
    vm.changeInventory = changeInventory;
    vm.changeReport = changeReport;
    vm.goToPersonal = goToPersonal;
    vm.goToDamageReport = goToDamageReport;
    vm.goToDepreciationReport = goToDepreciationReport;

    //functions
    vm.scanBarcode = scanBarcode;

    function scanBarcode() {
      $ionicPlatform.ready(function() {
        $cordovaBarcodeScanner
          .scan({
            prompt : "Place a barcode inside the scan area",
            resultDisplayDuration: 500,
            orientation : "portrait"
          })
          .then(function(imageData) {
            if(!imageData.cancelled){
              barcodeService.currentBarcode = imageData.text;
              $state.go('staff');
            }
            else{
              $state.go('home');
            }
        }, function(error) {
              $state.go('home');
        }
        );
      });
    };

    function scanRfid(){
      $state.go('staff');
    }

    function changeInventory(){
      vm.isInventory = true;
    }

    function changeReport(){
      vm.isInventory=false;
    }

    function goToPersonal(){
      $state.go('personalReport');
    }

    function goToDamageReport() {
      $state.go('damageReport');
    }

    function goToDepreciationReport() {
      $state.go('depreciationReport');
    }
  }
})();
